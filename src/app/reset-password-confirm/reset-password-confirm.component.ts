import { Component, OnInit } from '@angular/core';
import {routerTransition} from '../router.animations';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {AppConfig} from '../config/app.config';
import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {ResetPasswordConfirmData} from './model/reset-password-confirm.model';

@Component({
    selector: 'app-reset-password-confirm',
    templateUrl: './reset-password-confirm.component.html',
    styleUrls: ['./reset-password-confirm.component.scss'],
    animations: [routerTransition()]
})
export class ResetPasswordConfirmComponent implements OnInit {
    resetPasswordConfirmData: ResetPasswordConfirmData;
    serverUrl: String;
    resetPasswordConfirmForm: FormGroup;
    showResetSpinner: boolean;
    resetToken: string;

    constructor(private http: HttpClient,
                private toastr: ToastrService,
                private router: Router,
                private activeRouter: ActivatedRoute,
                private fb: FormBuilder,
                private translate: TranslateService) {
        this.resetPasswordConfirmData = new ResetPasswordConfirmData('', '');
        this.serverUrl = AppConfig.serverUrl;
        this.showResetSpinner = false;
        this.resetToken = this.activeRouter.params['_value']['token'];
        this.prepareUserRegisterForm();
    }

    ngOnInit() {
        sessionStorage.setItem('token', '');
    }

    prepareUserRegisterForm() {
        this.resetPasswordConfirmForm = this.fb.group({
            'newPassword': new FormControl('', Validators.required),
            'newRepeatedPassword': new FormControl('', Validators.required)
        });
    }

    sendResetPasswordRequest() {
        this.showResetSpinner = true;
        this.translate.get([
            'Reset password completed',
            'Success',
            'Could not change password',
            'Error'
        ]).subscribe((res: string) => {
            this.http.post(this.serverUrl + ':8082/api/users/password/' + this.resetToken, this.resetPasswordConfirmData).subscribe(
                (data) => {
                    this.toastr.success(res['Reset password completed'], res['Success']);
                    this.router.navigate(['/login']);
                    this.showResetSpinner = false;
                },
                error => {
                    this.toastr.error(res['Could not change password'], res['Error']);
                    this.showResetSpinner = false;
                });
        });
    }
}
