import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ResetPasswordConfirmComponent} from './reset-password-confirm.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PasswordModule} from 'primeng/password';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ResetPasswordConfirmRoutingModule} from './reset-password-confirm-routing.module';
import {ProgressSpinnerModule} from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule,
        ResetPasswordConfirmRoutingModule,
        FormsModule,
        PasswordModule,
        RouterModule,
        TranslateModule,
        ReactiveFormsModule,
        ProgressSpinnerModule
    ],
    declarations: [ResetPasswordConfirmComponent]
})
export class ResetPasswordConfirmModule {
}
