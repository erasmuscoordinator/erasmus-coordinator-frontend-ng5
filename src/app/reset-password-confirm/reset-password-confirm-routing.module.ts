import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ResetPasswordConfirmComponent} from './reset-password-confirm.component';

const routes: Routes = [
    {path: '', component: ResetPasswordConfirmComponent},
    {path: 'login', loadChildren: './../login/login.module#LoginModule'}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ResetPasswordConfirmRoutingModule {
}
