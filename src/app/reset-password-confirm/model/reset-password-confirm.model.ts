export class ResetPasswordConfirmData {
    constructor(public newPassword: string,
                public newRepeatedPassword: string) {}
}
