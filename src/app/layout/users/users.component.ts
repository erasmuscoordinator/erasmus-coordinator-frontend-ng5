import {Component, ElementRef, OnInit, ViewEncapsulation} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {HttpClient} from '@angular/common/http';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AppConfig} from '../../config/app.config';
import {ToastrService} from 'ngx-toastr';
import {User} from './models/user.model';
import {SelectItem} from 'primeng/primeng';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss'],
    animations: [routerTransition()],
    encapsulation: ViewEncapsulation.None
})
export class UsersComponent implements OnInit {
    closeResult: string;
    serverUrl: String;
    users: User[];
    displayChangeRole = false;
    currentUniversityUserChangeRole = 'STUDENT';
    currentUniversityUserChangeIndex: number;
    roles: SelectItem[];
    query: any;

    constructor(private http: HttpClient,
                private modalService: NgbModal,
                private toastr: ToastrService,
                private elRef: ElementRef,
                private translate: TranslateService) {
        this.serverUrl = AppConfig.serverUrl;
    }

    ngOnInit() {
        this.http.get<User[]>(this.serverUrl + ':8082/api/users/').subscribe(users => {
            this.users = users;
        });
        this.roles = [];
        this.roles.push({label: 'STUDENT', value: 'STUDENT'});
        this.roles.push({label: 'COORDINATOR', value: 'COORDINATOR'});
        this.roles.push({label: 'ADMINISTRATOR', value: 'ADMINISTRATOR'});
    }

    showDisplayChangeRoleDialog(index) {
        this.currentUniversityUserChangeRole = this.users[index].universityRole;
        this.currentUniversityUserChangeIndex = index;
        this.displayChangeRole = true;
    }

    saveUserChanges() {
        this.translate.get(['Role changed for given user', 'Could not change role for given user', 'Success', 'Error']).subscribe((res: string) => {
            this.http.put(this.serverUrl + ':8082/api/users/' + this.users[this.currentUniversityUserChangeIndex].id + '/role', {'universityRole': this.currentUniversityUserChangeRole}).subscribe(
                (data) => {
                    this.users[this.currentUniversityUserChangeIndex].universityRole = this.currentUniversityUserChangeRole;
                    this.toastr.success(res['Role changed for given user'], res['Success']);
                    this.displayChangeRole = false;
                },
                error => {
                    this.toastr.error(res['Could not change role for given user'], res['Error']);
                });
        });
    }

    open(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
}
