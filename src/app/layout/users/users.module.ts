import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

import { ChartsRoutingModule } from './users-routing.module';
import { PageHeaderModule } from '../../shared';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {SearchPipe} from './searchpipe.component';
import {UsersComponent} from './users.component';
import {TranslateModule} from '@ngx-translate/core';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule,
        Ng2Charts,
        ChartsRoutingModule,
        PageHeaderModule,
        NgbModule.forRoot(),
        FormsModule,
        TranslateModule,
        DialogModule,
        DropdownModule
    ],
    declarations: [UsersComponent, SearchPipe]
})
export class ChartsModule {}
