export class User {
    department: string;
    email: string;
    faculty: string;
    firstName: string;
    id: string;
    indexNumber: string;
    lastName: string;
    studiesDegree: string;
    university: string;
    universityRole: string;
    yearOfStudy: string;
}
