import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

import { ChartsRoutingModule } from './applications-routing.module';
import { ApplicationsComponent } from './applications.component';
import { PageHeaderModule } from '../../shared';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DndModule} from 'ng2-dnd';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
    imports: [
        CommonModule,
        Ng2Charts,
        ChartsRoutingModule,
        PageHeaderModule,
        NgbModule.forRoot(),
        DndModule.forRoot(),
        FormsModule,
        TranslateModule
    ],
    declarations: [
        ApplicationsComponent
    ]
})
export class ChartsModule {}
