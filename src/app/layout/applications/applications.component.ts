import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {HttpClient} from '@angular/common/http';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AppConfig} from '../../config/app.config';
import {ToastrService} from 'ngx-toastr';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import {SchemaGet} from '../schema-editor/models/schema-get.model';
import {Observable} from 'rxjs/Observable';
import {GroupedParams} from './models/grouped-params.model';
import UserApplication = namespace.UserApplication;
import ApplierData = namespace.ApplierData;
import Application = namespace.Application;
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: './applications.component.html',
    styleUrls: ['./applications.component.scss'],
    animations: [routerTransition()]
})
export class ApplicationsComponent implements OnInit {
    closeResult: string;
    applications: Application[];
    applierData: ApplierData;
    results: UserApplication;
    serverUrl: String;
    offerDataPatterns = ['department', 'endYear', 'startYear', 'vacancies'];
    offerDataCommonPatterns = ['code', 'universityName', 'country', 'coordinator', 'departmentCoordinator'];
    currentState: string;

    constructor(private http: HttpClient,
                private modalService: NgbModal,
                private toastr: ToastrService,
                private translate: TranslateService) {
        this.serverUrl = AppConfig.serverUrl;
        pdfMake.vfs = pdfFonts.pdfMake.vfs;
    }

    ngOnInit() {
        this.getApplications();
        this.getCurrentEnrollmentState();
    }

    private getApplications() {
        const userId = JSON.parse(sessionStorage.getItem('loggedUser'))['id'];
        this.http.get<UserApplication>(this.serverUrl + ':8080/api/applications/applier/' + userId).subscribe(data => {
            if (data != null) {
                this.results = data;
                this.applierData = data.applierData;
                this.applications = data.applications;
            }
        });
    }

    open(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    generatePdf(result) {
        this.downloadSchema().subscribe(
            (data) => {
                let parsedObject = {};
                if (data && data['content']) {
                    try {
                        parsedObject = JSON.parse(this.replaceSchemaTagsForSingleFile(data['content'], result.offerData, !result.applierFromTheSameDepartmentAsOffer, result.duration, result.semester));
                    } catch (e) {
                        this.toastr.error('Could not generate pdf', 'Error');
                        return;
                    }
                    pdfMake.createPdf(parsedObject).download();
                    return;
                }
                this.toastr.error('Cannot proceed schema generation', 'Server error');
            },
            error => {
                this.toastr.error('Problem with saving your preferences!!', 'Error');
            });
    }

    generatePdfFiles() {
        this.downloadSchema().subscribe(
            (data) => {
                let objectsToParse: string[] = [];
                if (data && data['content']) {
                    try {
                        const basicContent = this.replaceSchemaTagsWithoutCommonPatterns(data['content'], this.applications[0].offerData, !this.applications[0].applierFromTheSameDepartmentAsOffer);
                        objectsToParse = this.replaceGroupedSchemaTags(basicContent, this.prepareCommonGroups(this.applications));
                        for (let i = 0; i < objectsToParse.length; i++) {
                            pdfMake.createPdf(JSON.parse(objectsToParse[i])).download();
                        }
                        return;
                    } catch (e) {
                        this.toastr.error('Could not generate pdf', 'Error');
                        return;
                    }
                }
                this.toastr.error('Cannot proceed schema generation', 'Server error');
            },
            error => {
                this.toastr.error('Problem with saving your preferences!!', 'Error');
            });
    }

    downloadSchema(): Observable<Object> {
        return this.http.get<SchemaGet>(this.serverUrl + ':8080/api/schema/current');
    }

    prepareCommonGroups(applications) {
        if ((applications.length === 1)) {
            return [this.createGroupedParamsFromOfferDatas(
                [applications[0].offerData],
                [applications[0].duration],
                [applications[0].semester]
            )];
        }
        if ((applications.length === 2)) {
            if (applications[0].offerData.coordinator === applications[1].offerData.coordinator &&
                applications[0].offerData.departmentCoordinator === applications[1].offerData.departmentCoordinator) {
                return [this.createGroupedParamsFromOfferDatas(
                    [applications[1].offerData, applications[2].offerData],
                    [applications[1].duration, applications[2].duration],
                    [applications[1].semester, applications[2].semester]
                )];
            } else {
                return [
                    this.createGroupedParamsFromOfferDatas(
                    [applications[0].offerData],
                        [applications[0].duration],
                        [applications[0].semester]
                    ),
                    this.createGroupedParamsFromOfferDatas(
                        [applications[1].offerData],
                        [applications[1].duration],
                        [applications[1].semester]
                    )
                ];
            }
        }
        if ((applications.length === 3)) {
            if (applications[0].offerData.coordinator === applications[1].offerData.coordinator &&
                applications[0].offerData.coordinator === applications[2].offerData.coordinator &&
                applications[1].offerData.coordinator === applications[2].offerData.coordinator &&
                applications[0].offerData.departmentCoordinator === applications[1].offerData.departmentCoordinator &&
                applications[0].offerData.departmentCoordinator === applications[2].offerData.departmentCoordinator &&
                applications[1].offerData.departmentCoordinator === applications[2].offerData.departmentCoordinator) {
                return [
                    this.createGroupedParamsFromOfferDatas(
                    [applications[0].offerData, applications[1].offerData, applications[2].offerData],
                    [applications[0].duration, applications[1].duration, applications[2].duration],
                    [applications[0].semester, applications[1].semester, applications[2].semester]
                    )
                ];
            }
            if (applications[0].offerData.coordinator === applications[1].offerData.coordinator &&
                applications[0].offerData.departmentCoordinator === applications[1].offerData.departmentCoordinator) {
                return [
                    this.createGroupedParamsFromOfferDatas(
                        [applications[2].offerData],
                        [applications[2].duration],
                        [applications[2].semester]
                    ),
                    this.createGroupedParamsFromOfferDatas(
                        [applications[0].offerData, applications[1].offerData],
                        [applications[0].duration, applications[1].duration],
                        [applications[0].semester, applications[1].semester]
                    )
                ];
            }
            if (applications[0].offerData.coordinator === applications[2].offerData.coordinator &&
                applications[0].offerData.departmentCoordinator === applications[2].offerData.departmentCoordinator) {
                return [
                    this.createGroupedParamsFromOfferDatas(
                        [applications[1].offerData],
                        [applications[1].duration],
                        [applications[1].semster]
                    ),
                    this.createGroupedParamsFromOfferDatas(
                        [applications[0].offerData, applications[2].offerData],
                        [applications[0].duration, applications[2].duration],
                        [applications[0].semster, applications[2].semester]
                    )
                ];
            }
            if (applications[1].offerData.coordinator === applications[2].offerData.coordinator &&
                applications[1].offerData.departmentCoordinator === applications[2].offerData.departmentCoordinator) {
                return [
                    this.createGroupedParamsFromOfferDatas(
                        [applications[0].offerData],
                        [applications[0].duration],
                        [applications[0].semester
                        ]
                    ),
                    this.createGroupedParamsFromOfferDatas(
                        [applications[1].offerData, applications[2].offerData],
                        [applications[1].duration, applications[2].duration],
                        [applications[1].semester, applications[2].semester]
                    )
                ];
            }
            return  [
                this.createGroupedParamsFromOfferDatas(
                    [applications[0].offerData],
                    [applications[0].duration],
                    [applications[0].semester]
                ),
                this.createGroupedParamsFromOfferDatas(
                    [applications[1].offerData],
                    [applications[1].duration],
                    [applications[1].semester]
                ),
                this.createGroupedParamsFromOfferDatas(
                    [applications[2].offerData],
                    [applications[2].duration],
                    [applications[2].semester]
                )
            ];
        }
        return [];
    }

    createGroupedParamsFromOfferDatas(offerDatas, durations, semesters) {
        const groupedParams = new GroupedParams();
        groupedParams.coordinator = offerDatas[0].coordinator;
        groupedParams.departmentCoordinator = offerDatas[0].departmentCoordinator;
        groupedParams.codes = [];
        groupedParams.universityNames = [];
        groupedParams.countries = [];
        groupedParams.durations = [];
        groupedParams.semesters = [];
        for (let i = 0; i < offerDatas.length; i++) {
            groupedParams.codes.push(offerDatas[i].code);
            groupedParams.universityNames.push(offerDatas[i].universityName);
            groupedParams.countries.push(offerDatas[i].country);
        }
        groupedParams.durations = durations;
        groupedParams.semesters = semesters;
        return groupedParams;
    }

    replaceGroupedSchemaTags(basicContent, groupedParams) {
        let tmpTripleIdentifierForOffer = '';
        let tmpContent = '';
        let tmpDurations = '';
        let tmpSemesters = '';
        const resultsContents = [];
        for (let i = 0; i < groupedParams.length; i++) {
            tmpContent = basicContent;
            tmpTripleIdentifierForOffer = '';
            tmpDurations = '';
            tmpSemesters = '';
            for (let j = 0; j < groupedParams[i].countries.length; j++) {
                tmpTripleIdentifierForOffer += groupedParams[i].countries[j] + ', ' + groupedParams[i].universityNames[j] + ', ' + groupedParams[i].codes[j];
                tmpDurations += groupedParams[i].durations[j];
                tmpSemesters += groupedParams[i].semesters[j];
                if (j !== groupedParams[i].countries.length - 1) {
                    tmpTripleIdentifierForOffer +=  '\\n';
                    tmpDurations += ', ';
                    tmpSemesters += ', ';
                }
            }
            tmpContent = tmpContent.replace(new RegExp('<%country%>, <%universityName%>, <%code%>', 'g'), tmpTripleIdentifierForOffer);
            tmpContent = tmpContent.replace(new RegExp('<%coordinator%>', 'g'), groupedParams[i].coordinator);
            tmpContent = tmpContent.replace(new RegExp('<%departmentCoordinator%>', 'g'), groupedParams[i].departmentCoordinator);
            tmpContent = tmpContent.replace(new RegExp('<%duration%>', 'g'), tmpDurations);
            tmpContent = tmpContent.replace(new RegExp('<%sem%>', 'g'), tmpSemesters);
            resultsContents.push(tmpContent);
        }
        return resultsContents;
    }

    replaceSchemaTagsForSingleFile(schemaFeed, offerData, applierFromDifferentDepartmentAsOffer, duration, semester) {
        schemaFeed = schemaFeed.replace(new RegExp('<%duration%>', 'g'), this.normalizeParam(duration));
        schemaFeed = schemaFeed.replace(new RegExp('<%sem%>', 'g'), this.normalizeParam(semester));
        schemaFeed = this.replaceSchemaTagsWithoutCommonPatterns(schemaFeed, offerData, applierFromDifferentDepartmentAsOffer);
        for (let i = 0; i < this.offerDataCommonPatterns.length; i++) {
            schemaFeed = schemaFeed.replace(new RegExp('<%' + this.offerDataCommonPatterns[i] + '%>', 'g'), (offerData[this.offerDataCommonPatterns[i]] == null) ? '' : offerData[this.offerDataCommonPatterns[i]]);
        }
        return schemaFeed;
    }

    replaceSchemaTagsWithoutCommonPatterns(schemaFeed, offerData, applierFromDifferentDepartmentAsOffer) {
        for (let i = 0; i < this.offerDataPatterns.length; i++) {
            schemaFeed = schemaFeed.replace(new RegExp('<%' + this.offerDataPatterns[i] + '%>', 'g'), (offerData[this.offerDataPatterns[i]] == null) ? '' : offerData[this.offerDataPatterns[i]]);
        }
        schemaFeed = schemaFeed.replace(new RegExp('<%uLan%>', 'g'), this.normalizeStudentLanguages(this.applierData['language']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uLGr%>', 'g'), this.normalizeStudentLanguagesGrades(this.applierData['language']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uLCe%>', 'g'), this.normalizeStudentLanguageCertificates(this.applierData['language']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uAdd%>', 'g'), this.normalizeParam(this.applierData['address']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uDep%>', 'g'), this.normalizeParam(this.applierData['department']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uEma%>', 'g'), this.normalizeParam(this.applierData['email']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uFir%>', 'g'), this.normalizeParam(this.applierData['firstName']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uFac%>', 'g'), this.normalizeParam(this.applierData['faculty']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uGpa%>', 'g'), this.normalizeParam(this.applierData['gpa']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uHCS%>', 'g'), this.normalizeParam(this.applierData['completedSemester']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uHDi%>', 'g'), this.normalizeTrueFalseStatement(this.applierData['hasDisabilities']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uHSS%>', 'g'), this.normalizeTrueFalseStatement(this.applierData['hasSocialScholarship']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uINu%>', 'g'), this.normalizeParam(this.applierData['indexNumber']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uLNa%>', 'g'), this.normalizeParam(this.applierData['lastName']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uPho%>', 'g'), this.normalizeParam(this.applierData['phone']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uUni%>', 'g'), this.normalizeParam(this.applierData['university']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uYOS%>', 'g'), this.normalizeParam(this.applierData['yearOfStudy']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uFSD%>', 'g'), this.normalizeTrueFalseStatement(this.applierData['userOnFirstSemesterSecondDegree']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uSDe%>', 'g'), this.normalizeUserStudiesDegree(this.applierData['studiesDegree']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uFSO%>', 'g'), this.normalizeTrueFalseStatement(applierFromDifferentDepartmentAsOffer));
        schemaFeed = schemaFeed.replace(new RegExp('<%uEHP%>', 'g'), this.normalizeErasmusParticipation(this.applierData['erasmusParticipation']));
        schemaFeed = schemaFeed.replace(new RegExp('<%uEDe%>', 'g'), this.applierData['erasmusParticipation'] !== null && this.applierData['erasmusParticipation'] !== undefined ? this.applierData['erasmusParticipation'].studiesDegree : ' ');
        schemaFeed = schemaFeed.replace(new RegExp('<%uEMN%>', 'g'), this.applierData['erasmusParticipation'] !== null && this.applierData['erasmusParticipation'] !== undefined ? this.applierData['erasmusParticipation'].duration : ' ');
        return schemaFeed;
    }

    normalizeErasmusParticipation(erasmusParticipation) {
        if (erasmusParticipation.yearOfStudy !== null && erasmusParticipation.yearOfStudy !== undefined && erasmusParticipation.yearOfStudy !== '') {
            return 'tak';
        }
        if (erasmusParticipation.studiesDegree !== null && erasmusParticipation.studiesDegree !== undefined && erasmusParticipation.studiesDegree !== '') {
            return 'tak';
        }
        if (erasmusParticipation.duration !== null && erasmusParticipation.duration !== undefined && erasmusParticipation.duration !== '') {
            return 'tak';
        }
        return 'nie';
    }

    private normalizeUserStudiesDegree(param) {
        if (param === null || param === undefined || param === 'NONE') {
            return ' ';
        } else if (param === 'BACHELOR') {
            return 'inżynierskie';
        } else if (param === 'MASTERS') {
            return 'magisterskie';
        } else if (param === 'DOCTORATE') {
            return 'doktoranckie';
        } else if (param === 'POST_GRADUATE') {
            return 'podyplomowe';
        }
        return param;
    }

    private normalizeParam(param) {
        if (param === null || param === undefined) {
            return ' ';
        }
        return param;
    }

    private normalizeStudentLanguages(languages) {
        if (languages && languages.length > 0) {
            let languageTmp = ' ';
            if (languages[0]['name'] !== undefined && languages[0]['name'] !== null) {
                languageTmp += languages[0]['name'] + '\\n';
            }
            if (languages.length > 1 && (languages[1]['name'] !== undefined && languages[1]['name'] !== null)) {
                languageTmp += languages[1]['name'];
            }
            return languageTmp;
        }
        return ' ';
    }

    private normalizeStudentLanguagesGrades(languages) {
        if (languages && languages.length > 0) {
            let languageTmp = ' ';
            if (languages[0]['examLevel'] !== undefined && languages[0]['examLevel'] !== null) {
                languageTmp += languages[0]['examLevel'] + '\\n';
            }
            if (languages.length > 1 && (languages[1]['examLevel'] !== undefined && languages[1]['examLevel'] !== null)) {
                languageTmp += languages[1]['examLevel'];
            }
            return languageTmp;
        }
        return ' ';
    }

    normalizeStudentLanguageCertificates(languages) {
        if (languages && languages.length > 0) {
            let languageTmp = ' ';
            if (languages[0]['certificateType'] !== undefined && languages[0]['certificateType'] !== null) {
                languageTmp += languages[0]['certificateType'] + '\\n';
            }
            if (languages.length > 1 && (languages[1]['certificateType'] !== undefined && languages[1]['certificateType'] !== null)) {
                languageTmp += languages[1]['certificateType'];
            }
            return languageTmp;
        }
        return ' ';
    }

    normalizeTrueFalseStatement(statement) {
        if (statement === null || statement === undefined) {
            return ' ';
        }
        if (statement) {
            return 'tak';
        }
        return 'nie';
    }

    savePriorityList() {
        this.http.post(this.serverUrl + ':8080/api/applications/', this.buildApplicationRequestBody()).subscribe(
            (data) => {
                this.toastr.success('Changes saved!', 'Success');
            },
            error => {
                this.toastr.error('Problem with saving your preferences!!', 'Error');
            });
    }

    private buildApplicationRequestBody() {
        const applications = [];
        for (let i = 0; i < this.results['applications'].length; i++) {
            const obj = {};
            obj['universityOfferId'] = this.results['applications'][i].universityOfferId;
            obj['offerData'] = this.results['applications'][i].offerData;
            applications.push(obj);
        }
        const result = {};
        result['applications'] = applications;
        result['applierData'] = JSON.parse(sessionStorage.getItem('loggedUser'));
        result['applierId'] = JSON.parse(sessionStorage.getItem('loggedUser'))['id'];
        return result;
    }

    getRequiredStyles(status) {
        let myStyles;
        if (status === 'APPROVED') {
            myStyles = {
                'background-color': '#5bf9058f'
            };
        } else if (status === 'EXCLUDED') {
            myStyles = {
                'background-color': '#e633338f'
            };
        }
        return myStyles;
    }

    getCurrentEnrollmentState() {
        this.http.get(this.serverUrl + ':8080/api/state/current/').subscribe(
            (data) => {
                this.currentState = data['enrollmentState'];
            },
            error => {
                this.toastr.error('Problem with loading current state!', 'Server error');
            });
    }

    isWieitStudent() {
        return JSON.parse(sessionStorage.getItem('loggedUser'))['department'] && JSON.parse(sessionStorage.getItem('loggedUser'))['department'].toLowerCase() === 'wieit';
    }

    userHasNotSpecifiedDepartment() {
        return JSON.parse(sessionStorage.getItem('loggedUser'))['department'] === null || JSON.parse(sessionStorage.getItem('loggedUser'))['department'] === undefined;
    }
}
