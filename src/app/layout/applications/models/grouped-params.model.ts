export class GroupedParams {
    coordinator: string;
    departmentCoordinator: string;
    countries: Array<string>;
    universityNames: Array<string>;
    codes: Array<string>;
    durations: Array<string>;
    semesters: Array<string>;
}
