declare module namespace {

    export interface OfferData {
        code: string;
        coordinator: string;
        coordinatorId: string;
        country: string;
        department: string;
        duration: string;
        endYear: string;
        departmentCoordinator: string;
        departmentCoordinatorId: string;
        id: string;
        startYear: string;
        universityName: string;
        vacancies: string;
    }

    export interface Application {
        applierFromTheSameDepartmentAsOffer: boolean;
        duration: string;
        offerData: OfferData;
        preference: number;
        status: string;
        universityOfferId: string;
        isApplierFromTheSameDepartmentAsOffer: boolean;
    }

    export interface ErasmusParticipation {
        duration: string;
        studiesDegree: string;
        yearOfStudy: string;
    }

    export interface Language {
        certificateType: string;
        examLevel: string;
        grade: string;
        name: string;
    }

    export interface ApplierData {
        address: string;
        completedSemester: string;
        department: string;
        email: string;
        erasmusParticipation: ErasmusParticipation;
        faculty: string;
        firstName: string;
        gpa: number;
        hasDisabilities: boolean;
        hasPowerScholarship: boolean;
        hasSocialScholarship: boolean;
        id: string;
        indexNumber: string;
        language: Language[];
        lastName: string;
        phone: string;
        studiesDegree: string;
        university: string;
        userOnFirstSemesterSecondDegree: boolean;
        yearOfStudy: string;
        isUserOnFirstSemesterSecondDegree: boolean;
    }

    export interface UserApplication {
        applications: Application[];
        applierData: ApplierData;
        applierId: string;
        approvedByDwz: boolean;
        created: Date;
        id: string;
    }
}

