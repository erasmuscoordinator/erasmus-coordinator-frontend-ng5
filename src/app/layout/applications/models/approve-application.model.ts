export class ApproveApplication {
    constructor(public id: String,
                public approvedByCoordinator: boolean) {}
}
