import {Component, ElementRef, OnInit, ViewEncapsulation} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {HttpClient} from '@angular/common/http';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AppConfig} from '../../config/app.config';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-root',
    templateUrl: './offers.component.html',
    styleUrls: ['./offers.component.scss'],
    animations: [routerTransition()],
    encapsulation: ViewEncapsulation.None
})
export class OffersComponent implements OnInit {
    closeResult: string;
    isCoordinator: boolean;
    serverUrl: String;
    agreements: any[] = [];
    query = '';

    constructor(private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService, private elRef: ElementRef) {
        this.serverUrl = AppConfig.serverUrl;
    }

    ngOnInit() {
        const user = JSON.parse(sessionStorage.getItem('loggedUser'));
        this.isCoordinator = user['universityRole'] === 'COORDINATOR';
        const query = this.isCoordinator ? 'coordinators?coordinator=' + user['lastName'] : 'departments?department=WIEiT';
        this.http.get(this.serverUrl + ':8081/agreements/search/' + query + '&sort=country&size=150').subscribe(agreements => {
            this.agreements = agreements['_embedded'].agreements;
        });
    }

    open(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

}
