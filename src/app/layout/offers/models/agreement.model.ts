export class Agreement {
    constructor(public code: String,
                public coordinator: String,
                public coordinatorId: String,
                public country: String,
                public department: String,
                public duration: String,
                public endYear: String,
                public departmentCoordinator: String,
                public departmentCoordinatorId: String,
                public id: String,
                public startYear: String,
                public universityName: String,
                public vacancies: String) {}
}
