import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

import { ChartsRoutingModule } from './offers-routing.module';
import { OffersComponent } from './offers.component';
import { PageHeaderModule } from '../../shared';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from "@angular/forms";
import {SearchPipe} from "./searchpipe.component";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
    imports: [CommonModule, Ng2Charts, ChartsRoutingModule, PageHeaderModule, NgbModule.forRoot(), FormsModule, TranslateModule],
    declarations: [OffersComponent, SearchPipe]
})
export class ChartsModule {}
