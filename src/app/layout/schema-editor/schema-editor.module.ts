import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AceEditorModule} from 'ng2-ace-editor';
import {PageHeaderModule} from '../../shared';
import {FormsModule} from '@angular/forms';
import {SafePipe} from './safe-pipe.component';
import {NgModule} from '@angular/core';
import {SchemaEditorComponent} from './schema-editor.component';
import {SchemaEditorRoutingModule} from './schema-editor-routing.module';
import {TabViewModule} from 'primeng/tabview';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService, PanelModule} from 'primeng/primeng';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        SchemaEditorRoutingModule,
        PageHeaderModule,
        NgbModule.forRoot(),
        AceEditorModule,
        FormsModule,
        TabViewModule,
        ConfirmDialogModule,
        TranslateModule,
        PanelModule
    ],
    declarations: [SchemaEditorComponent, SafePipe],
    providers: [ConfirmationService]
})
export class SchemaEditorModule {
}
