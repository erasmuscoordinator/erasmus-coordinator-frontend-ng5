export class SchemaSave {
    constructor(public modifiedUserName: String,
                public content: String,
                public modificationReason: String) {}
}
