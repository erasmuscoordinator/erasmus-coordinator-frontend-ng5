export class SchemaGet {
    constructor(public modifiedByUsername: String,
                public content: string,
                public modificationReason: String,
                public modified: String,
                public schemaId: String) {}
}
