import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import {AppConfig} from '../../config/app.config';
import {HttpClient} from '@angular/common/http';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';

import 'brace/index';
import 'brace/theme/eclipse';
import 'brace/mode/typescript';
import 'brace/mode/javascript';
import 'brace/ext/language_tools.js';
import {SchemaSave} from './models/schema-save.model';
import {SchemaGet} from './models/schema-get.model';
import {ConfirmationService} from 'primeng/api';
import {TranslateService} from '@ngx-translate/core';

declare var ace: any;

@Component({
    selector: 'app-root',
    templateUrl: './schema-editor.component.html',
    styleUrls: ['./schema-editor.component.scss'],
    animations: [routerTransition()]
})
export class SchemaEditorComponent implements OnInit {
    serverUrl: String;
    pdfInBase64: String;
    historicalPdfInBase64: String;
    schemaContent: string;
    isValid: boolean;
    schema: SchemaSave;
    historicalSchemas: Array<SchemaGet>;
    selectedRow: number;
    historicalContent: string;

    constructor(private http: HttpClient,
                private modalService: NgbModal,
                private toastr: ToastrService,
                private confirmationService: ConfirmationService,
                private translate: TranslateService) {
        pdfMake.vfs = pdfFonts.pdfMake.vfs;
        this.serverUrl = AppConfig.serverUrl;
        this.isValid = true;
        this.schema = new SchemaSave('', '', '');
        this.historicalContent = '';
        this.getLatestSchema();
    }

    ngOnInit() {
    }

    onChange(event) {
        this.updatePdf();
    }

    getLatestSchema() {
        this.http.get<SchemaGet>(this.serverUrl + ':8080/api/schema/current').subscribe((data) => {
            if (data == null) {
                this.getDefaultSchema();
                return;
            }
            this.schemaContent = data.content;
        });
    }

    updatePdf() {
        let parsedObject = {};
        if (this.schemaContent) {
            try {
                parsedObject = JSON.parse(this.schemaContent);
            } catch (e) {
                if (this.isValid) {
                    this.toastr.warning('Problem with validating. Correct your schema!', 'Warning');
                    this.isValid = false;
                }
                return;
            }
        }
        if (!this.isValid) {
            this.isValid = true;
            this.toastr.success('Schema is now valid and updated!', 'Success');
        }
        const pdfDocGenerator = pdfMake.createPdf(parsedObject);
        pdfDocGenerator.getBase64((data) => {
            this.pdfInBase64 = data;
        });
    }

    getPdfBase64Content() {
        return 'data:application/pdf;base64,' + this.pdfInBase64;
    }

    getPdfBase64ContentForHistoricalSchema() {
        return 'data:application/pdf;base64,' + this.historicalPdfInBase64;
    }

    saveSchema() {
        if (!this.isValid) {
            this.toastr.warning('Schema is not valid. Cannot save!', 'Warning');
            return;
        }
        this.schema.content = this.schemaContent;
        this.schema.modifiedUserName = JSON.parse(sessionStorage.getItem('loggedUser'))['firstName'] +
            ' ' +
            JSON.parse(sessionStorage.getItem('loggedUser'))['lastName'];
        this.http.post(this.serverUrl + ':8080/api/schema/current', this.schema).subscribe(
            (data) => {
                this.toastr.success('Schema updated!', 'Success');
            },
            error => {
                this.toastr.error('Problem with updating schema!!', 'Server error');
            });
    }

    getDefaultSchema() {
        this.http.get<string>('assets/data/default-schema.json').subscribe(
            (data) => {
                this.schemaContent = JSON.stringify(data, null, '\t');
            },
            error => {
                this.schemaContent = '{\n' + '\t"content": []\n}';
                this.toastr.error('Problem with loading default schema!!', 'Server error');
            });
    }

    getHistoricalSchemas(event) {
        this.http.get<Array<SchemaGet>>(this.serverUrl + ':8080/api/schema/historical').subscribe(
            (data) => {
                this.historicalSchemas = data;
            },
            error => {
                this.toastr.error('Problem with loading historical schemas!!', 'Server error');
            });
    }

   setClickedRow(index) {
       this.selectedRow = index;
       const pdfDocGenerator = pdfMake.createPdf(JSON.parse(this.historicalSchemas[index].content));
       pdfDocGenerator.getBase64((data) => {
           this.historicalPdfInBase64 = data;
       });
       this.historicalContent = this.historicalSchemas[index].content;
   }

    confirmSaveSchema() {
        if (this.isValid) {
            this.translate.get(['Are you sure that you want to modify schema']).subscribe((res: string) => {
                this.confirmationService.confirm({
                    message: res['Are you sure that you want to modify schema'] + '?',
                    accept: () => {
                        this.saveSchema();
                    }
                });
            });
        }
    }
}
