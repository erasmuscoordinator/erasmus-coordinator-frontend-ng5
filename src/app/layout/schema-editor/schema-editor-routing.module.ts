import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SchemaEditorComponent} from './schema-editor.component';

const routes: Routes = [
    {
        path: '',
        component: SchemaEditorComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SchemaEditorRoutingModule {}
