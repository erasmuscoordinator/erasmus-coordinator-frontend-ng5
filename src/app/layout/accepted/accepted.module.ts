import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

import { ChartsRoutingModule } from './accepted-routing.module';
import { AcceptedComponent } from './accepted.component';
import { PageHeaderModule } from '../../shared';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from "@angular/forms";
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
    imports: [CommonModule, Ng2Charts, ChartsRoutingModule, PageHeaderModule, NgbModule.forRoot(), FormsModule, TranslateModule],
    declarations: [AcceptedComponent]
})
export class ChartsModule {}
