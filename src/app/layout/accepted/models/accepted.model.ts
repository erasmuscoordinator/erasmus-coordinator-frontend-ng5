export interface AdditionalParameters {
}

export interface OfferData {
    additionalParameters: AdditionalParameters;
    code: string;
    coordinator: string;
    coordinatorId: string;
    country: string;
    department: string;
    duration: string;
    endYear: string;
    departmentCoordinator: string;
    departmentCoordinatorId: string;
    formSchema: string;
    id: string;
    startYear: string;
    universityName: string;
    vacancies: string;
}

export interface Application {
    applierFromTheSameDepartmentAsOffer: boolean;
    offerData: OfferData;
    preference: number;
    status: string;
    universityOfferId: string;
    isApplierFromTheSameDepartmentAsOffer: boolean;
}

export interface ErasmusParticipation {
    duration: string;
    studiesDegree: string;
    yearOfStudy: string;
}

export interface Language {
    certificateType: string;
    examLevel: string;
    grade: string;
    name: string;
}

export interface ApplierData {
    address: string;
    department: string;
    email: string;
    erasmusParticipation: ErasmusParticipation;
    faculty: string;
    firstName: string;
    gpa: number;
    completedSemester: string;
    hasDisabilities: boolean;
    hasSocialScholarship: boolean;
    hasPowerScholarship: boolean;
    id: string;
    indexNumber: string;
    language: Language[];
    lastName: string;
    phone: string;
    studiesDegree: string;
    university: string;
    userOnFirstSemesterSecondDegree: boolean;
    yearOfStudy: string;
    isUserOnFirstSemesterSecondDegree: boolean;
}

export interface AcceptedModel {
    application: Application;
    applierData: ApplierData;
    userApplicationsId: string;
}
