import {AfterViewChecked, Component, ElementRef, OnInit, ViewEncapsulation} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {HttpClient} from '@angular/common/http';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AppConfig} from '../../config/app.config';
import {ToastrService} from 'ngx-toastr';
import {AcceptedModel} from './models/accepted.model';
import {TableExport} from 'tableexport';


@Component({
    selector: 'app-root',
    templateUrl: './accepted.component.html',
    styleUrls: ['./accepted.component.scss'],
    animations: [routerTransition()],
    encapsulation: ViewEncapsulation.None
})
export class AcceptedComponent implements OnInit, AfterViewChecked {
    wieitDepartment: boolean;
    serverUrl: String;
    acceptedEntriesWieit: AcceptedModel[];
    acceptedEntriesOther: AcceptedModel[];
    displayedEntries: AcceptedModel[];
    selectedDisplayPolicy: String;
    tableExport: TableExport;

    currentState: string;

    constructor(private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService, private elRef: ElementRef) {
        this.serverUrl = AppConfig.serverUrl;
    }

    ngOnInit() {
        this.getCurrentEnrollmentState();
        const loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
        const isCoordinator = loggedUser['universityRole'] === 'COORDINATOR';
        const approvedEndpoint = isCoordinator ? ('approved?approvedByDwz=true' + '&coordinatorName=' + loggedUser['lastName'] + '&') : ('approved?');
        const request = ':8080/api/applications/' + approvedEndpoint;

        this.http.get<AcceptedModel[]>(this.serverUrl + request + 'department=wieit').subscribe(accepted => {
            this.acceptedEntriesWieit = accepted;
            this.displayedEntries = this.acceptedEntriesWieit;
        });
        this.http.get<AcceptedModel[]>(this.serverUrl + request + 'department=nonWieit').subscribe(accepted => {
            this.acceptedEntriesOther = accepted;
        });
        this.wieitDepartment = true;
        this.selectedDisplayPolicy = 'Tylko wydział WIEiT';
    }

    changeDisplay(event) {
        if (event === 'Tylko wydział WIEiT') {
            this.displayedEntries = this.acceptedEntriesWieit;
            this.wieitDepartment = true;
        } else {
            this.displayedEntries = this.acceptedEntriesOther;
            this.wieitDepartment = false;
        }
    }

    ngAfterViewChecked() {
        new TableExport(document.getElementsByTagName('table'), {
            filename: 'ErasmusAccepted',                // (id, String), filename for the downloaded file, (default: 'id')
            bootstrap: false,                            // (Boolean), style buttons using bootstrap, (default: true)
            position: 'bottom',
            trimWhitespace: true
        }).reset();
    }

    getCurrentEnrollmentState() {
        this.http.get(this.serverUrl + ':8080/api/state/current/').subscribe(
            (data) => {
                this.currentState = data['enrollmentState'];
            },
            error => {
                this.toastr.error('Problem with loading current state!', 'Server error');
            });
    }
}
