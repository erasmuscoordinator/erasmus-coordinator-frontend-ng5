import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
    isActive: Boolean = false;
    showMenu: String = '';

    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    isStudent() {
        return JSON.parse(sessionStorage.getItem('loggedUser'))['universityRole'] === 'STUDENT';
    }

    isCoordinator() {
        return JSON.parse(sessionStorage.getItem('loggedUser'))['universityRole'] === 'COORDINATOR';
    }

    isAdmin() {
        return JSON.parse(sessionStorage.getItem('loggedUser'))['universityRole'] === 'ADMINISTRATOR';
    }
}
