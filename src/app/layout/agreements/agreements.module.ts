import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChartsModule as Ng2Charts} from 'ng2-charts';

import {ChartsRoutingModule} from './agreements-routing.module';
import {AgreementsComponent} from './agreements.component';
import {PageHeaderModule} from '../../shared';

import {ModalComponent} from './components/modal/modal.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PickListModule} from 'primeng/picklist';
import {TranslateModule} from '@ngx-translate/core';
import {StepsModule} from 'primeng/steps';
import {MenuItem} from 'primeng/api';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SearchPipe} from './searchpipe.component';
import {SpinnerModule} from 'primeng/spinner';

@NgModule({
    imports: [
        CommonModule,
        Ng2Charts,
        ChartsRoutingModule,
        PageHeaderModule,
        NgbModule.forRoot(),
        PickListModule,
        StepsModule,
        TranslateModule,
        FormsModule,
        SpinnerModule,
        ReactiveFormsModule
    ],
    declarations: [
        AgreementsComponent,
        ModalComponent,
        SearchPipe
    ]
})
export class ChartsModule {}
