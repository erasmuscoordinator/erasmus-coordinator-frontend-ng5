import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {HttpClient, HttpParams, HttpRequest} from '@angular/common/http';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AppConfig} from '../../config/app.config';
import {ToastrService} from 'ngx-toastr';
import {MenuItem} from 'primeng/primeng';
import {ErasmusParticipation, Language, User} from '../profile/models/user.model';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
    selector: 'app-root',
    templateUrl: './agreements.component.html',
    styleUrls: ['./agreements.component.scss'],
    animations: [routerTransition()],
    encapsulation: ViewEncapsulation.None
})
export class AgreementsComponent implements OnInit {
    closeResult: string;
    serverUrl: String;
    sourceAgreements: any[] = [];
    targetAgreements: any[] = [];
    targetAgreementsDuration: number[] = [6, 6, 6];
    targetAgreementsSemesters: string[] = ['', '', ''];
    isAdmin = false;
    users: User[];

    // profile
    profileData: User;
    isStudent = true;
    isPrevParticipationCollapsed = false;
    isLanguagesCollapsed = false;
    firstCertificateFile: any;
    secondCertificateFile: any;
    profileForm: FormGroup;

    // steps
    items: MenuItem[];
    activeIndex = 0;

    currentState: string;

    constructor(private http: HttpClient,
                private modalService: NgbModal,
                private toastr: ToastrService,
                private translate: TranslateService,
                private router: Router,
                private fb: FormBuilder) {
        this.serverUrl = AppConfig.serverUrl;
    }

    ngOnInit() {
        this.getCurrentEnrollmentState();
        const loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
        this.isAdmin = loggedUser['universityRole'] === 'ADMINISTRATOR';

        if (!this.isAdmin) {
            this.profileData = loggedUser;
            this.getNewestUserData(loggedUser);
            this.getAgreements();
        }

        if (this.isAdmin) {
            this.http.get<User[]>(this.serverUrl + ':8082/api/users/?universityRole=STUDENT').subscribe(users => {
                this.users = users;
            });
        }

        this.profileForm = this.fb.group({
            'university': new FormControl('', Validators.required),
            'department': new FormControl('', Validators.required),
            'faculty': new FormControl('', Validators.required)
        });

        this.activeIndex = 0;
        this.prepareSteps();
    }

    prepareSteps() {
        this.translate.get(['Choose agreements', 'Confirm personal data', 'Choose user', 'for user', 'Specify duration in months']).subscribe((res: string) => {
            if (this.isAdmin) {
                this.items = [
                    {
                        label: res['Choose user']
                    },
                    {
                        label: res['Choose agreements'] + ' ' + res['for user']
                    },
                    {
                        label: res['Specify duration in months'] + ' ' + res['for user']
                    },
                    {
                        label: res['Confirm personal data'] + ' ' + res['for user']
                    }
                ];

            } else {
                this.items = [
                    {
                        label: res['Choose agreements']
                    },
                    {
                        label: res['Specify duration in months']
                    },
                    {
                        label: res['Confirm personal data']
                    }
                ];
            }
        });
    }

    getNewestUserData(targetedUser) {
        this.http.get<User>(this.serverUrl + ':8082/api/users/' + targetedUser.id).subscribe((data) => {
            if (data.erasmusParticipation == null) {
                data.erasmusParticipation = new ErasmusParticipation();
                data.erasmusParticipation.duration = '';
                data.erasmusParticipation.studiesDegree = '';
                data.erasmusParticipation.yearOfStudy = '';
            }
            if (data.language == null) {
                data.language = [new Language(), new Language()];
            }
            if (data.studiesDegree === null || data.studiesDegree === undefined || data.studiesDegree === 'NONE') {
                data.studiesDegree = 'BACHELOR';
            }
            if (data.yearOfStudy === null || data.yearOfStudy === undefined || data.yearOfStudy === '') {
                data.yearOfStudy = '1';
            }
            if (data.completedSemester === null || data.completedSemester === undefined || data.completedSemester === '') {
                data.completedSemester = '1';
            }
            if (data.department === null || data.department === undefined || data.department === '') {
                data.department = 'WIEiT';
            }
            this.profileData = data;
            if (data.department !== null && data.department !== undefined && data.department.toLocaleLowerCase() === 'wieit') {
                this.profileData.faculty = 'Informatyka';
            }
            this.isStudent = this.profileData.universityRole === 'STUDENT';
            if (data.universityRole === 'STUDENT') {
                this.profileForm = this.fb.group({
                    'university': new FormControl('', Validators.required),
                    'department': new FormControl('', Validators.required),
                    'faculty': new FormControl('', Validators.required),
                    'indexNumber': new FormControl('', Validators.required),
                    'studiesDegree': new FormControl('', Validators.required),
                    'yearOfStudy': new FormControl('', Validators.required),
                    'completedSemester': new FormControl('', Validators.required),
                    'gpa': new FormControl('', Validators.required),
                    'address': new FormControl('', Validators.required),
                    'phone': new FormControl('', Validators.required)
                });
            }
        });
    }

    private getAgreements() {
        this.http.get(this.serverUrl + ':8081/agreements/search/departments?department=WIEiT&sort=country&size=150').subscribe(agreements => {
            this.sourceAgreements = agreements['_embedded'].agreements;
            this.http.get<Object[]>(this.serverUrl + ':8080/api/applications/applier/' + this.profileData.id).subscribe(data => {
                if (data != null) {
                    this.adjustSourceAgreements(data['applications'], agreements['_embedded'].agreements);
                }
            });
        });
    }

    adjustSourceAgreements(applications, agreements) {
        const tmpSourceAgreements = [];
        for (let i = 0; i < applications.length; i++) {
            this.targetAgreements.push(applications[i].offerData);
        }
        for (let i = 0; i < agreements.length; i++) {
            let addToTarget = true;
            for (let j = 0; j < applications.length; j++) {
                if (applications[j].universityOfferId === agreements[i].id) {
                    addToTarget = false;
                }
            }
            if (addToTarget) {
                tmpSourceAgreements.push(agreements[i]);
            }
        }
        this.sourceAgreements = tmpSourceAgreements;
    }

    open(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    chooseUser(user: User) {
        this.profileData = user;
        this.getNewestUserData(user);
        this.activeIndex += 1;
        this.getAgreements();
    }

    prevIndex() {
        if (!this.isFirstIndex()) {
            this.activeIndex -= 1;
        }
    }

    isFirstIndex() {
        return this.activeIndex <= 0;
    }

    nextIndex() {
        if (this.targetAgreements.length > 3) {
            this.toastr.error('Cannot apply on more than 3 offers!', 'Validation problem');
        } else if (!this.isLastIndex()) {
            this.activeIndex += 1;
        }
    }

    isLastIndex() {
        return this.activeIndex >= this.items.length - 1;
    }

    savePriorityList() {
        if (this.targetAgreements.length > 3) {
            this.toastr.error('Cannot apply on more than 3 offers!', 'Validation problem');
        } else {
            if (this.firstCertificateFile) {
                this.sendCertificate(this.firstCertificateFile, 'first_certificate');
            }
            if (this.secondCertificateFile) {
                this.sendCertificate(this.secondCertificateFile, 'second_certificate');
            }
            this.http.put(this.serverUrl + ':8082/api/users/', this.profileData).subscribe(
                (data) => {
                    if (!this.isAdmin) {
                        sessionStorage.setItem('loggedUser', JSON.stringify(this.profileData));
                    }
                },
                error => {
                    this.toastr.error('Problem with saving your profile!!', 'Error');
                });

            this.http.post(this.serverUrl + ':8080/api/applications/', this.buildApplicationRequestBody()).subscribe(
                (data) => {
                    this.toastr.success('Changes saved!', 'Success');
                    if (!this.isAdmin) {
                        this.router.navigate(['/applications']);
                    } else {
                        this.router.navigate(['/matrix']);
                    }
                },
                error => {
                    this.toastr.error('Problem with saving your preferences!!', 'Error');
                });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    private buildApplicationRequestBody() {
        const applications = [];
        for (let i = 0; i < this.targetAgreements.length; i++) {
            const obj = {};
            obj['universityOfferId'] = this.targetAgreements[i].id;
            obj['duration'] = this.targetAgreementsDuration[i] + ' ' + this.normalizeMonthsName(this.targetAgreementsDuration[i]);
            obj['semester'] = this.targetAgreementsSemesters[i];
            applications.push(obj);
        }
        const result = {};
        result['applications'] = applications;
        result['applierData'] = this.profileData;
        result['applierId'] = this.profileData.id;
        return result;
    }

    normalizeMonthsName(numberOfMonths) {
        if (numberOfMonths === 1) {
            return 'miesiąc';
        } else if (numberOfMonths > 1 && numberOfMonths < 5) {
            return 'miesiące';
        } else {
            return 'miesiący';
        }
    }

    sendCertificate(certificateFile, endpointEnd) {
        const formData = new FormData();
        formData.append('file', certificateFile);
        const params = new HttpParams();
        const options = {
            params: params,
            reportProgress: true,
        };
        const req = new HttpRequest('POST', this.serverUrl + ':8082/api/users/' + JSON.parse(sessionStorage.getItem('loggedUser'))['id'] + '/image/' + endpointEnd, formData, options);
        this.http.request(req).subscribe(
            (data) => {
            },
            error => {
                this.toastr.error('Problem with saving your profile!!', 'Error');
            });
    }

    setFirstCertificate(event) {
        if (event.target.files.length === 1) {
            this.firstCertificateFile = event.target.files[0];
        }
    }

    setSecondCertificate(event) {
        if (event.target.files.length === 1) {
            this.secondCertificateFile = event.target.files[0];
        }
    }

    getCurrentEnrollmentState() {
        this.http.get(this.serverUrl + ':8080/api/state/current/').subscribe(
            (data) => {
                this.currentState = data['enrollmentState'];
            },
            error => {
                this.toastr.error('Problem with loading current state!', 'Server error');
            });
    }
}
