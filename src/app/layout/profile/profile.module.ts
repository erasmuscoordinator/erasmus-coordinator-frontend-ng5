import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChartsModule as Ng2Charts} from 'ng2-charts';
import {ChartsRoutingModule} from './profile-routing.module';
import {ProfileComponent} from './profile.component';
import {PageHeaderModule} from '../../shared';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ConfirmationService, ConfirmDialogModule} from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule,
        Ng2Charts,
        ChartsRoutingModule,
        PageHeaderModule,
        NgbModule.forRoot(),
        TranslateModule,
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        ConfirmDialogModule
    ],
    declarations: [ProfileComponent],
    providers: [ConfirmationService]
})
export class ChartsModule {
}
