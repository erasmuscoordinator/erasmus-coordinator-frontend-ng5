import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {HttpClient, HttpParams, HttpRequest} from '@angular/common/http';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ErasmusParticipation, Language, User} from './models/user.model';
import {AppConfig} from '../../config/app.config';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ConfirmationService} from 'primeng/primeng';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    animations: [routerTransition()]
})
export class ProfileComponent implements OnInit {
    closeResult: string;
    profileData: User;
    serverUrl: String;
    isPrevParticipationCollapsed = false;
    isLanguagesCollapsed = false;
    isStudent = false;
    firstCertificateFile: any;
    secondCertificateFile: any;
    profileForm: FormGroup;
    hasApplications = false;

    constructor(private http: HttpClient,
                private modalService: NgbModal,
                private toastr: ToastrService,
                private fb: FormBuilder,
                private confirmationService: ConfirmationService,
                private translate: TranslateService) {
        this.serverUrl = AppConfig.serverUrl;
        this.profileData = new User();
        this.profileData.department = '';
    }

    ngOnInit() {
        this.profileForm = this.fb.group({
            'university': new FormControl('', Validators.required),
            'department': new FormControl('', Validators.required),
            'faculty': new FormControl('', Validators.required)
        });
        const loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
        this.http.get<User>(this.serverUrl + ':8082/api/users/' + loggedUser.id).subscribe((data) => {
            if (data.erasmusParticipation == null) {
                data.erasmusParticipation = new ErasmusParticipation();
                data.erasmusParticipation.duration = '';
                data.erasmusParticipation.studiesDegree = '';
                data.erasmusParticipation.yearOfStudy = '';
            }
            if (data.language == null) {
                data.language = [new Language(), new Language()];
            }
            if (data.studiesDegree === null || data.studiesDegree === undefined || data.studiesDegree === 'NONE') {
                data.studiesDegree = 'BACHELOR';
            }
            if (data.yearOfStudy === null || data.yearOfStudy === undefined || data.yearOfStudy === '') {
                data.yearOfStudy = '1';
            }
            if (data.completedSemester === null || data.completedSemester === undefined || data.completedSemester === '') {
                data.completedSemester = '1';
            }
            if (data.department === null || data.department === undefined || data.department === '') {
                data.department = 'WIEiT';
            }
            this.profileData = data;
            if (data.department !== null  && data.department !== undefined && data.department.toLocaleLowerCase() === 'wieit') {
                this.profileData.faculty = 'Informatyka';
            }
            this.isStudent = this.profileData.universityRole === 'STUDENT';
            if (data.universityRole === 'STUDENT') {
                this.profileForm = this.fb.group({
                    'university': new FormControl('', Validators.required),
                    'department': new FormControl('', Validators.required),
                    'faculty': new FormControl('', Validators.required),
                    'indexNumber': new FormControl('', Validators.required),
                    'studiesDegree': new FormControl('', Validators.required),
                    'yearOfStudy': new FormControl('', Validators.required),
                    'completedSemester': new FormControl('', Validators.required),
                    'gpa': new FormControl('', Validators.required),
                    'address': new FormControl('', Validators.required),
                    'phone': new FormControl('', Validators.required)
                });
            }
            if (this.isStudent) {
                this.http.get(this.serverUrl + ':8080/api/applications/applier/' + loggedUser.id).subscribe(data => {
                    if (data != null) {
                        this.hasApplications = true;
                    }
                });
            }
        });
    }

    open(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    saveClicked() {
        this.http.put(this.serverUrl + ':8082/api/users/', this.profileData).subscribe(
            (data) => {
                if (this.firstCertificateFile) {
                    this.sendCertificate(this.firstCertificateFile, 'first_certificate');
                }
                if (this.secondCertificateFile) {
                    this.sendCertificate(this.secondCertificateFile, 'second_certificate');
                }
                this.toastr.success('Changes saved!', 'Success');
            },
            error => {
                this.toastr.error('Problem with saving your profile!!', 'Error');
            });
        sessionStorage.setItem('loggedUser', JSON.stringify(this.profileData));
    }

    sendCertificate(certificateFile, endpointEnd) {
        const formData = new FormData();
        formData.append('file', certificateFile);
        const params = new HttpParams();
        const options = {
            params: params,
            reportProgress: true,
        };
        const req = new HttpRequest('POST', this.serverUrl + ':8082/api/users/' + JSON.parse(sessionStorage.getItem('loggedUser'))['id'] + '/image/' + endpointEnd, formData, options);
        this.http.request(req).subscribe(
            (data) => {
            },
            error => {
                this.toastr.error('Problem with saving your certificates', 'Error');
            });
    }

    setFirstCertificate(event) {
        if (event.target.files.length === 1) {
            this.firstCertificateFile = event.target.files[0];
        }
    }

    setSecondCertificate(event) {
        if (event.target.files.length === 1) {
            this.secondCertificateFile = event.target.files[0];
        }
    }

    confirmSaveProfile() {
        this.translate.get(['Are you sure that you want to modify your profile']).subscribe((res: string) => {
            this.confirmationService.confirm({
                message: res['Are you sure that you want to modify your profile'] + '?',
                accept: () => {
                    this.saveClicked();
                }
            });
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
}
