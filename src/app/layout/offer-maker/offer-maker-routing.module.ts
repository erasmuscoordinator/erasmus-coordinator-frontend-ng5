import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OfferMakerComponent} from './offer-maker.component';

const routes: Routes = [
    {
        path: '',
        component: OfferMakerComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OfferMakerRoutingModule {
}
