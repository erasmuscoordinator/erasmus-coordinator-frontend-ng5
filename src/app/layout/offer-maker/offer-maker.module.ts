import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OfferMakerRoutingModule} from './offer-maker-routing.module';
import {PageHeaderModule} from '../../shared';
import {OfferMakerComponent} from './offer-maker.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AceEditorModule} from 'ng2-ace-editor';
import {TranslateModule} from '@ngx-translate/core';
import {AccordionModule } from 'primeng/accordion';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PanelModule} from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule,
        OfferMakerRoutingModule,
        PageHeaderModule,
        NgbModule.forRoot(),
        AceEditorModule,
        FormsModule,
        TranslateModule,
        AccordionModule,
        PanelModule,
        ReactiveFormsModule
    ],
    declarations: [OfferMakerComponent],
})
export class OfferMakerModule {
}
