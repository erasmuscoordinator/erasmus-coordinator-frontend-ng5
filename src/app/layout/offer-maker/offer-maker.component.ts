import {Component, OnInit} from '@angular/core';
import {AppConfig} from '../../config/app.config';
import {HttpClient} from '@angular/common/http';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';

import 'brace/index';
import 'brace/theme/eclipse';
import 'brace/mode/typescript';
import 'brace/mode/javascript';
import 'brace/ext/language_tools.js';
import {Agreement} from '../agreements/models/agreement.model';
import {routerTransition} from '../../router.animations';
import {Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';

@Component({
    selector: 'app-root',
    templateUrl: './offer-maker.component.html',
    styleUrls: ['./offer-maker.component.scss'],
    animations: [routerTransition()]
})
export class OfferMakerComponent implements OnInit {
    private serverUrl: String;
    private offerAgreement: Agreement;
    private offerForm: FormGroup;
    currentState: string;

    constructor(private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService, private fb: FormBuilder) {
        this.serverUrl = AppConfig.serverUrl;
        this.offerAgreement = new Agreement('',
            JSON.parse(sessionStorage.getItem('loggedUser'))['firstName'] + ' ' + JSON.parse(sessionStorage.getItem('loggedUser'))['lastName'],
            '', 'WIEiT', '', '',
            JSON.parse(sessionStorage.getItem('loggedUser'))['firstName'] + ' ' + JSON.parse(sessionStorage.getItem('loggedUser'))['lastName'],
            '', '', '', '');
    }

    ngOnInit() {
        this.getCurrentEnrollmentState();
        this.offerForm = this.fb.group({
            'code': new FormControl('', Validators.required),
            'departmentCoordinator': new FormControl('', Validators.required),
            'agreementCoordinator': new FormControl('', Validators.required),
            'universityName': new FormControl('', Validators.required),
            'country': new FormControl('', Validators.required),
            'department': new FormControl('', Validators.required),
            'duration': new FormControl('', Validators.required)
        });
    }

    saveOffer() {
        this.http.post(this.serverUrl + ':8081/agreements', this.offerAgreement).subscribe(
            (data) => {
                this.toastr.success('Offer saved!', 'Success');
            },
            error => {
                this.toastr.error('Problem with saving your offer!!', 'Server error');
            });
    }

    getCurrentEnrollmentState() {
        this.http.get(this.serverUrl + ':8080/api/state/current/').subscribe(
            (data) => {
                this.currentState = data['enrollmentState'];
            },
            error => {
                this.toastr.error('Problem with loading current state!', 'Server error');
            });
    }
}
