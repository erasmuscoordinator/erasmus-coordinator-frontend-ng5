import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'agreements', loadChildren: './agreements/agreements.module#ChartsModule' },
            { path: 'offers', loadChildren: './offers/offers.module#ChartsModule' },
            { path: 'users', loadChildren: './users/users.module#ChartsModule' },
            { path: 'applications', loadChildren: './applications/applications.module#ChartsModule' },
            { path: 'profile', loadChildren: './profile/profile.module#ChartsModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'matrix', loadChildren: './matrix/matrix.module#MatrixModule' },
            { path: 'accepted', loadChildren: './accepted/accepted.module#ChartsModule' },
            { path: 'offer-maker', loadChildren: './offer-maker/offer-maker.module#OfferMakerModule' },
            { path: 'schema-editor', loadChildren: './schema-editor/schema-editor.module#SchemaEditorModule' },
            { path: 'dwz', loadChildren: './dwz-list/dwz.module#ChartsModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
