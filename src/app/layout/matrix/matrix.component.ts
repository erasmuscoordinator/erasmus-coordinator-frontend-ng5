import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {HttpClient} from '@angular/common/http';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {AppConfig} from '../../config/app.config';

@Component({
    selector: 'app-grid',
    templateUrl: './matrix.component.html',
    styleUrls: ['./matrix.component.scss'],
    animations: [routerTransition()]
})
export class MatrixComponent implements OnInit {
    serverUrl: String;
    matrixEndpoint: String;
    isCoordinator = false;

    // matrix
    matrix: Object[];
    displayedMatrix: Object[];
    selectedDisplayPolicy: String;
    usersWithMoreThanOneApplication: Object[];

    // modal
    displayedUserApplicationsName: String;
    displayedUserApplications: Object[];
    closeResult: string;

    currentState: string;

    constructor(private http: HttpClient, private modalService: NgbModal, private toastr: ToastrService) {
        this.serverUrl = AppConfig.serverUrl;
    }

    ngOnInit() {
        this.getCurrentEnrollmentState();
        const loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
        this.isCoordinator = loggedUser['universityRole'] === 'COORDINATOR';
        this.matrixEndpoint = this.isCoordinator ? '/api/matrix/coordinator/' + loggedUser['lastName'] : '/api/matrix/';
        this.updateMatrix();
    }

    updateMatrix() {
        this.selectedDisplayPolicy = 'Only agreements with forms';
        this.http.get<Object[]>(this.serverUrl + ':8080' + this.matrixEndpoint).subscribe(matrix => {
            this.matrix = matrix;
            this.displayedMatrix = [];
            for (let i = 0; i < matrix.length; i++) {
                if (matrix[i]['applications'].length > 0) {
                    this.displayedMatrix.push(matrix[i]);
                }
            }
        });
    }

    hasMoreThanOneApplicationThatAreNotExcluded(application) {
        for (let i = 0; i < this.matrix.length; i++) {
            for (let j = 0; j < this.matrix[i]['applications'].length; j++) {
                if (this.matrix[i]['applications'][j].user.id === application.user.id &&
                    application.application.offerData !== undefined &&
                    this.matrix[i]['offer'].id !== application.application.offerData.id &&
                    this.matrix[i]['applications'][j].application.status !== 'EXCLUDED') {
                    return true;
                }
            }
        }
        return false;
    }

    undoDeclineGivenApplication(application) {
        application.application.status = 'UNKNOWN';
        for (let i = 0; i < this.matrix.length; i++) {
            for (let j = 0; j < this.matrix[i]['applications'].length; j++) {
                if (this.matrix[i]['applications'][j].user.id === application.user.id &&
                    application.application.offerData !== undefined &&
                    this.matrix[i]['offer'].id === application.application.offerData.id) {
                    this.matrix[i]['applications'][j].status = 'UNKNOWN';
                }
            }
        }
      }

    declineGivenApplication(application) {
        application.application.status = 'EXCLUDED';
        for (let i = 0; i < this.matrix.length; i++) {
            for (let j = 0; j < this.matrix[i]['applications'].length; j++) {
                if (this.matrix[i]['applications'][j].user.id === application.user.id &&
                    application.application.offerData !== undefined &&
                    this.matrix[i]['offer'].id === application.application.offerData.id) {
                    this.matrix[i]['applications'][j].status = 'EXCLUDED';
                }
            }
        }
    }

    changeDisplay(event) {
        if (event === 'All agreements') {
            this.displayedMatrix = this.matrix;
        } else if (event === 'Only agreements without forms') {
            const tmpMatrix = [];
            for (let i = 0; i < this.matrix.length; i++) {
                if (this.matrix[i]['applications'].length === 0) {
                    tmpMatrix.push(this.matrix[i]);
                }
            }
            this.displayedMatrix = tmpMatrix;
        } else {
            const tmpMatrix = [];
            for (let i = 0; i < this.matrix.length; i++) {
                if (this.matrix[i]['applications'].length > 0) {
                    tmpMatrix.push(this.matrix[i]);
                }
            }
            this.displayedMatrix = tmpMatrix;
        }
    }

    isMatrixValid() {
        for (let i = 0; i < this.matrix.length; i++) {
            for (let j = 0; j < this.matrix[i]['applications'].length; j++) {
                if (this.matrix[i]['applications'][j].application.status !== 'EXCLUDED' && this.hasMoreThanOneApplicationThatAreNotExcluded(this.matrix[i]['applications'][j])) {
                    return false;
                }
            }
        }
        return true;
    }

    sendUpdatedMatrix() {
        if (this.isMatrixValid()) {
            this.http.put(this.serverUrl + ':8080/api/matrix/', this.matrix).subscribe(
                (data) => {
                    this.toastr.success('Matrix saved!', 'Success');
                    this.updateMatrix();
                },
                error => {
                    this.toastr.error('Server error during matrix update', 'Error');
                });
        } else {
            this.showUsersWithMoreThanOneApplication();
        }
    }

    showUsersWithMoreThanOneApplication() {
        this.usersWithMoreThanOneApplication = [];
        for (let i = 0; i < this.matrix.length; i++) {
            for (let j = 0; j < this.matrix[i]['applications'].length; j++) {
                if (this.matrix[i]['applications'][j].application.status !== 'EXCLUDED' &&
                    this.hasMoreThanOneApplicationThatAreNotExcluded(this.matrix[i]['applications'][j]) &&
                    this.checkIfDoesNotContainUserInArray(this.matrix[i]['applications'][j].user)) {
                    this.usersWithMoreThanOneApplication.push(this.matrix[i]['applications'][j].user);
                }
            }
        }
        const users = this.buildStringWithUserWithMoreThanOneApplication();
        this.toastr.warning(
            'There are still students with more than one application. Please adjust: ' + users,
            'Warning',
            {enableHtml: true, timeOut: 10000});
    }

    checkIfDoesNotContainUserInArray(user) {
        for (let i = 0; i < this.usersWithMoreThanOneApplication.length; i++) {
            if (this.usersWithMoreThanOneApplication[i]['id'] === user.id) {
                return false;
            }
        }
        return true;
    }

    buildStringWithUserWithMoreThanOneApplication() {
        let result = '<br><br>';
        for (let i = 0; i < this.usersWithMoreThanOneApplication.length; i++) {
            result += '- ' + this.usersWithMoreThanOneApplication[i]['firstName'] + ' ' + this.usersWithMoreThanOneApplication[i]['lastName'] + '<br>';
        }
        return result;
    }

    // modal
    openUserApplicationsModal(content, applicationData) {
        this.updateSelectedUserApplications(applicationData);
        this.modalService.open(content, { size: 'lg' }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    updateSelectedUserApplications(applicationData) {
        this.displayedUserApplications = [];
        this.displayedUserApplicationsName = applicationData.user.firstName + ' ' + applicationData.user.lastName;
        for (let i = 0; i < this.matrix.length; i++) {
            for (let j = 0; j < this.matrix[i]['applications'].length; j++) {
                if (this.matrix[i]['applications'][j].user.id === applicationData.user.id) {
                    this.displayedUserApplications.push(this.matrix[i]['applications'][j]);
                }
            }
        }
    }

    updateMatrixWithSuggestedMatrix() {
        this.http.get<Object[]>(this.serverUrl + ':8080/api/matrix/suggested').subscribe(matrix => {
            this.matrix = matrix;
            this.displayedMatrix = [];
            for (let i = 0; i < matrix.length; i++) {
                if (matrix[i]['applications'].length > 0) {
                    this.displayedMatrix.push(matrix[i]);
                }
            }
            this.selectedDisplayPolicy = 'Only agreements with forms';
            this.toastr.info('Matrix updated with suggested preferences by applicants', 'Success');
        });
    }

    getCurrentEnrollmentState() {
        this.http.get(this.serverUrl + ':8080/api/state/current/').subscribe(
            (data) => {
                this.currentState = data['enrollmentState'];
            },
            error => {
                this.toastr.error('Problem with loading current state!', 'Server error');
            });
    }

}
