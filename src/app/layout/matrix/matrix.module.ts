import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MatrixRoutingModule} from './matrix-routing.module';
import {MatrixComponent} from './matrix.component';
import {PageHeaderModule} from './../../shared';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
    imports: [CommonModule, MatrixRoutingModule, PageHeaderModule, NgbModule.forRoot(), FormsModule, TranslateModule],
    declarations: [MatrixComponent]
})
export class MatrixModule {
}
