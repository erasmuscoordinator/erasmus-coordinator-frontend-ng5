import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {AppConfig} from '../../config/app.config';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {ConfirmationService, DialogModule, MenuItem} from 'primeng/primeng';
import {TranslateService} from '@ngx-translate/core';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()],
    encapsulation: ViewEncapsulation.None

})
export class DashboardComponent implements OnInit {
    isStudent: boolean;
    isAdmin = false;
    private serverUrl: String;
    private applications: any[] = [];
    private agreements: {};
    private users: any[] = [];
    private currentState: string;
    private editionName: string;
    displayEndEditionDialog: boolean;

    // steps
    items: MenuItem[];
    activeIndex = -1;

    constructor(private http: HttpClient, private toastr: ToastrService, private translate: TranslateService, private confirmationService: ConfirmationService, private dialogModule: DialogModule) {
        this.serverUrl = AppConfig.serverUrl;
        this.agreements = {totalElements: 0};
        this.translate.get(['BEFORE_ENROLLMENT', 'ENROLLMENT', 'QUALIFICATIONS', 'DWZ_APPROVALS']).subscribe((res: string) => {
            this.items = [
                {
                    label: res['BEFORE_ENROLLMENT']
                },
                {
                    label: res['ENROLLMENT']
                },
                {
                    label: res['QUALIFICATIONS']
                },
                {
                    label: res['DWZ_APPROVALS']
                }
            ];
        });
    }

    ngOnInit() {
        this.isStudent = JSON.parse(sessionStorage.getItem('loggedUser'))['universityRole'] === 'STUDENT';
        this.isAdmin = JSON.parse(sessionStorage.getItem('loggedUser'))['universityRole'] === 'ADMINISTRATOR';
        this.displayEndEditionDialog = this.currentState === 'DWZ_APPROVALS';

        this.http.get(this.serverUrl + ':8081/agreements/search/departments?department=WIEiT').subscribe(data => {
            this.agreements = data['page'];
        });

        if (this.isStudent) {
            const userId = JSON.parse(sessionStorage.getItem('loggedUser'))['id'];
            this.http.get(this.serverUrl + ':8080/api/applications/applier/' + userId).subscribe(data => {
                if (data != null) {
                    this.applications = data['applications'];
                }
            });
        } else if (this.isAdmin) {
            this.http.get(this.serverUrl + ':8080/api/applications/').subscribe(data => {
                this.applications = [].concat(data);
            });
            this.http.get(this.serverUrl + ':8082/api/users/').subscribe(data => {
                this.users = [].concat(data);
            });
        }
        this.getCurrentEnrollmentState();
    }

    getCurrentEnrollmentState() {
        this.http.get(this.serverUrl + ':8080/api/state/current/').subscribe(
            (data) => {
                this.currentState = data['enrollmentState'];
                if (this.currentState === 'BEFORE_ENROLLMENT') {
                    this.activeIndex = 0;
                } else if (this.currentState === 'ENROLLMENT') {
                    this.activeIndex = 1;
                } else if (this.currentState === 'QUALIFICATIONS') {
                    this.activeIndex = 2;
                } else if (this.currentState === 'DWZ_APPROVALS') {
                    this.activeIndex = 3;
                }
            },
            error => {
                this.toastr.error('Problem with loading current state!', 'Server error');
            });
    }

    incrementState() {
        if (this.currentState !== 'DWZ_APPROVALS') {
            this.http.put(this.serverUrl + ':8080/api/state/increment', {}).subscribe(
                (data) => {
                    this.getCurrentEnrollmentState();
                    this.activeIndex += 1;
                },
                error => {
                    this.toastr.error('Problem with changing current state!', 'Server error');
                });
        }
    }

    decrementState() {
        if (this.currentState !== 'BEFORE_ENROLLMENT') {
            this.http.put(this.serverUrl + ':8080/api/state/decrement', {}).subscribe(
                (data) => {
                    this.getCurrentEnrollmentState();
                    this.activeIndex -= 1;
                },
                error => {
                    this.toastr.error('Problem with changing current state!', 'Server error');
                });
        }
    }

    setState(state: string) {
        this.http.put(this.serverUrl + ':8080/api/state/', {'enrollmentState': state}).subscribe(
            (data) => {
                this.getCurrentEnrollmentState();
            },
            error => {
                this.toastr.error('Problem with changing current state!', 'Server error');
            });
    }

    confirmIncrementState() {
        this.translate.get(['Are you sure that you want change to next state?']).subscribe((res: string) => {
            this.confirmationService.confirm({
                message: res['Are you sure that you want change to next state?'],
                accept: () => {
                    this.incrementState();
                }
            });
        });
    }

    confirmDecrementState() {
        this.translate.get(['Are you sure that you want change to previous state?']).subscribe((res: string) => {
            this.confirmationService.confirm({
                message: res['Are you sure that you want change to previous state?'],
                accept: () => {
                    this.decrementState();
                }
            });
        });
    }

    showEndEditionDialog() {
        this.displayEndEditionDialog = true;
    }

    sendEndEditionRequest() {
        this.http.post(this.serverUrl + ':8080/api/archive', this.buildRequestBody()).subscribe(
            (data) => {
                this.toastr.success('Changes saved!', 'Success');
                this.displayEndEditionDialog = false;
                this.setState('BEFORE_ENROLLMENT');
            },
            error => {
                if (error.status == 400) {
                    this.toastr.error('Edition name already exists!', 'User error');
                } else {
                    this.toastr.error('Problem with loading current state!', 'Server error');
                }
            });
    }

    buildRequestBody() {
        const result = {};
        result['edition'] = this.editionName;
        return result;
    }

}
