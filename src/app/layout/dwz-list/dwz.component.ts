import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../config/app.config';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {AcceptedModel} from '../accepted/models/accepted.model';


@Component({
    selector: 'app-root',
    templateUrl: './dwz.component.html',
    styleUrls: ['./dwz.component.scss'],
    animations: [routerTransition()],
    encapsulation: ViewEncapsulation.None
})
export class DwzComponent implements OnInit {
    serverUrl: String;
    sourceApplications: AcceptedModel[] = [];
    targetApplications: AcceptedModel[] = [];

    currentState: string;

    constructor(private http: HttpClient, private toastr: ToastrService, private translate: TranslateService) {
        this.serverUrl = AppConfig.serverUrl;
    }

    ngOnInit() {
        this.getCurrentEnrollmentState();
        this.http.get<AcceptedModel[]>(this.serverUrl + ':8080/api/applications/approved?approvedByDwz=false').subscribe(accepted => {
            this.sourceApplications = accepted;
        });
        this.http.get<AcceptedModel[]>(this.serverUrl + ':8080/api/applications/approved?approvedByDwz=true').subscribe(accepted => {
            this.targetApplications = accepted;
        });
    }

    savePriorityList() {
        this.buildRequestBody(this.targetApplications, true);
        this.http.put(this.serverUrl + ':8080/api/applications/approve/', this.buildRequestBody(this.targetApplications, true)).subscribe(
            (data) => {
                this.toastr.success('Changes saved!', 'Success');
            },
            error => {
                this.toastr.error('Problem with saving approved forms!!', 'Error');
            });

        this.http.put(this.serverUrl + ':8080/api/applications/approve/', this.buildRequestBody(this.sourceApplications, false)).subscribe(
            (data) => {},
            error => {
                this.toastr.error('Problem with saving not approved forms!!', 'Error');
            });
    }

    private buildRequestBody(applications: AcceptedModel[], approvedByDwz: boolean) {
        const ids: String[] = applications.map(function (application) {
            return application.userApplicationsId;
        });
        const result = {};
        result['ids'] = ids;
        result['approvedByDwz'] = approvedByDwz;
        return result;
    }

    getCurrentEnrollmentState() {
        this.http.get(this.serverUrl + ':8080/api/state/current/').subscribe(
            (data) => {
                this.currentState = data['enrollmentState'];
            },
            error => {
                this.toastr.error('Problem with loading current state!', 'Server error');
            });
    }
}
