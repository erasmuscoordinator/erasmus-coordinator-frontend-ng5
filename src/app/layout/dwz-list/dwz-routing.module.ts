import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DwzComponent} from './dwz.component';

const routes: Routes = [
    {
        path: '',
        component: DwzComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ChartsRoutingModule {
}
