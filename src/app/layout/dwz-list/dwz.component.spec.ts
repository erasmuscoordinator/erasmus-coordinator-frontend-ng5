import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DwzComponent } from './dwz.component';

describe('DwzComponent', () => {
    let component: DwzComponent;
    let fixture: ComponentFixture<DwzComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                declarations: [DwzComponent]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(DwzComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
