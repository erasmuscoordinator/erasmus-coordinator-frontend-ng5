import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChartsModule as Ng2Charts} from 'ng2-charts';

import {ChartsRoutingModule} from './dwz-routing.module';
import {DwzComponent} from './dwz.component';
import {PageHeaderModule} from '../../shared';

import {ModalComponent} from './components/modal/modal.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PickListModule} from 'primeng/picklist';
import {TranslateModule} from '@ngx-translate/core';
import {MenuItem} from 'primeng/api';

@NgModule({
    imports: [
        CommonModule,
        Ng2Charts,
        ChartsRoutingModule,
        PageHeaderModule,
        NgbModule.forRoot(),
        PickListModule,
        TranslateModule
    ],
    declarations: [
        DwzComponent,
        ModalComponent
    ]
})
export class ChartsModule {
}
