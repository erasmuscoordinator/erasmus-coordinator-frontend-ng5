import {Agreement} from './agreement.model';
import {User} from '../../../login/models/user.model';

export class Application {
    constructor(public universityOfferId: String,
                public applierId: String,
                public offerData: Agreement,
                public applierData: User) {}
}
