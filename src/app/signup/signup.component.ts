import { Component, OnInit } from '@angular/core';
import {routerTransitionToLeft} from '../router.animations';
import {User} from './models/user.model';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {AppConfig} from '../config/app.config';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransitionToLeft()]
})
export class SignupComponent implements OnInit {
    userToRegister: User;
    serverUrl: String;
    profileRegisterForm: FormGroup;

    constructor(private http: HttpClient,
                private toastr: ToastrService,
                private router: Router,
                private fb: FormBuilder,
                private translate: TranslateService) {
        this.userToRegister = new User('', '', '', '');
        this.serverUrl = AppConfig.serverUrl;
        this.prepareUserRegisterForm();
    }

    ngOnInit() {
        sessionStorage.setItem('token', '');
    }

    prepareUserRegisterForm() {
        this.profileRegisterForm = this.fb.group({
            'email': new FormControl('', Validators.email),
            'firstName': new FormControl('', Validators.required),
            'lastName': new FormControl('', Validators.required),
            'password': new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(100)])
        });
    }

    registerUser() {
        this.translate.get(['Registration complete', 'Success', 'Could not register user', 'Error']).subscribe((res: string) => {
            this.http.post(this.serverUrl + ':8082/api/users/', this.userToRegister).subscribe(
                (data) => {
                    this.toastr.success(res['Registration complete'], res['Success']);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.toastr.error(res['Could not register user'], res['Error']);
                });
        });
    }
}
