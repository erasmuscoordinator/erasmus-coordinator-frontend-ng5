import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SignupRoutingModule} from './signup-routing.module';
import {SignupComponent} from './signup.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PasswordModule} from 'primeng/password';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        SignupRoutingModule,
        FormsModule,
        PasswordModule,
        RouterModule,
        TranslateModule,
        ReactiveFormsModule
    ],
    declarations: [SignupComponent]
})
export class SignupModule {
}
