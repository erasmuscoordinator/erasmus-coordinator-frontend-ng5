import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor, HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import {Router} from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private router: Router) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        request = request.clone({
            setHeaders: {
                Authorization: `${sessionStorage.getItem('token')}`
            }
        });

        return next.handle(request).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                // do stuff with response if you want
            }
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                if ((err.status === 500 && err.error.message.includes('JWT'))
                    || err.status === 401
                    || err.status === 403) {
                    this.onLoggedout();
                    this.router.navigate(['/login']);
                }
            }
        });
    }

    onLoggedout() {
        sessionStorage.removeItem('isLoggedin');
        sessionStorage.removeItem('loggedUser');
        sessionStorage.removeItem('token');
    }
}
