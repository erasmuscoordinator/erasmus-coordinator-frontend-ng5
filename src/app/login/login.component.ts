import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {routerTransitionToRight} from '../router.animations';
import {HttpClient} from '@angular/common/http';
import {User} from './models/user.model';
import {AppConfig} from '../config/app.config';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {UserLogin} from './models/user-login.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransitionToRight()]
})
export class LoginComponent implements OnInit {
    userLoginModel: UserLogin;
    loggedUser: User;
    serverUrl: String;
    showLoggingSpinner: boolean;
    profileLoginForm: FormGroup;

    private loggedSuccessfullyMsg: string;
    private loginFailedMsg: string;
    private successMsg: string;
    private failedMsg: string;

    constructor(public router: Router,
                private httpClient: HttpClient,
                private toastr: ToastrService,
                private fb: FormBuilder,
                private translate: TranslateService) {
        this.serverUrl = AppConfig.serverUrl;
        this.showLoggingSpinner = false;
        this.userLoginModel = new UserLogin('', '');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|pl/) ? browserLang : 'pl');
        this.prepareUserRegisterForm();
    }

    ngOnInit() {
        this.translate.get(['Logged successfully to system', 'Wrong user name or password!', 'Welcome', 'Cannot login']).subscribe((res: string) => {
            this.loggedSuccessfullyMsg = res['Logged successfully to system'];
            this.loginFailedMsg = res['Wrong user name or password!'];
            this.successMsg = res['Welcome'];
            this.failedMsg = res['Cannot login'];
        });
        sessionStorage.setItem('token', '');
    }

    prepareUserRegisterForm() {
        this.profileLoginForm = this.fb.group({
            'email': new FormControl('', Validators.email),
            'password': new FormControl('', Validators.required)
        });
    }

    onLoggedin() {
        this.showLoggingSpinner = true;
        this.httpClient.post<User>(this.serverUrl + ':8082/login', this.userLoginModel, {observe: 'response'}).subscribe(
            resp => {
                this.showLoggingSpinner = false;
                const token = resp.headers.get('Authorization');
                sessionStorage.setItem('token', token);
                this.loggedUser = resp.body;
                sessionStorage.setItem('loggedUser', JSON.stringify(resp.body));
                sessionStorage.setItem('isLoggedin', 'true');
                this.toastr.success(this.loggedSuccessfullyMsg, `${this.successMsg} ${this.loggedUser.firstName}`);
                this.showLoggingSpinner = false;
                this.router.navigate(['/dashboard']);

            },
            error => {
                this.toastr.error(this.loginFailedMsg, this.failedMsg);
                this.showLoggingSpinner = false;
            });
    }
}
