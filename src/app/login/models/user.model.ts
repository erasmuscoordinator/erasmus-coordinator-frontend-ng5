export interface Language {
    name: string;
    examLevel: string;
    grade: string;
    certificateType: string;
}

export interface ErasmusParticipation {
    yearOfStudy: string;
    studiesDegree: string;
    duration: string;
}

export class User {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
    university: string;
    department: string;
    faculty: string;
    indexNumber: string;
    yearOfStudy: string;
    studiesDegree: string;
    universityRole: string;
    address: string;
    phone: string;
    gpa: number;
    language: Language[];
    erasmusParticipation: ErasmusParticipation;
    completedSemester: string;
    hasSocialScholarship: boolean;
    hasDisabilities: boolean;
    userOnFirstSemesterSecondDegree: boolean;
}
