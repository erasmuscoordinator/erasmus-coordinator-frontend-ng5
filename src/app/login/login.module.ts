import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {LoginRoutingModule} from './login-routing.module';
import {LoginComponent} from './login.component';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule,
        FormsModule,
        ProgressSpinnerModule,
        TranslateModule,
        ReactiveFormsModule
    ],
    declarations: [LoginComponent]
})
export class LoginModule {
}
