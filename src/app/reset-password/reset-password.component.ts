import { Component, OnInit } from '@angular/core';
import {routerTransitionToLeft} from '../router.animations';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {AppConfig} from '../config/app.config';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {ResetPasswordData} from './model/reset-password.model';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss'],
    animations: [routerTransitionToLeft()]
})
export class ResetPasswordComponent implements OnInit {
    resetPasswordData: ResetPasswordData;
    serverUrl: String;
    resetPasswordForm: FormGroup;
    showResetSpinner: boolean;

    constructor(private http: HttpClient,
                private toastr: ToastrService,
                private router: Router,
                private fb: FormBuilder,
                private translate: TranslateService) {
        this.resetPasswordData = new ResetPasswordData('');
        this.serverUrl = AppConfig.serverUrl;
        this.showResetSpinner = false;
        this.prepareUserRegisterForm();
    }

    ngOnInit() {
        sessionStorage.setItem('token', '');
    }

    prepareUserRegisterForm() {
        this.resetPasswordForm = this.fb.group({
            'email': new FormControl('', Validators.email)
        });
    }

    sendResetPasswordRequest() {
        this.showResetSpinner = true;
        this.translate.get([
            'Reset password request completed, please check your email',
            'Success',
            'Could not send reset password request',
            'Error',
            'Please wait',
            'Processing password reset request'
        ]).subscribe((res: string) => {
            this.toastr.info(res['Processing password reset request'], res['Please wait']);
            this.http.post(this.serverUrl + ':8082/api/users/password', this.resetPasswordData).subscribe(
                (data) => {
                    this.toastr.success(res['Reset password request completed, please check your email'], res['Success']);
                    this.router.navigate(['/login']);
                    this.showResetSpinner = false;
                },
                error => {
                    this.toastr.error(res['Could not send reset password request'], res['Error']);
                    this.showResetSpinner = false;
                });
        });
    }
}
