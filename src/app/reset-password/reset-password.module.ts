import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ResetPasswordComponent} from './reset-password.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PasswordModule} from 'primeng/password';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ResetPasswordRoutingModule} from './reset-password-routing.module';
import {ProgressSpinnerModule} from 'primeng/primeng';

@NgModule({
    imports: [
        CommonModule,
        ResetPasswordRoutingModule,
        FormsModule,
        PasswordModule,
        RouterModule,
        TranslateModule,
        ReactiveFormsModule,
        ProgressSpinnerModule
    ],
    declarations: [ResetPasswordComponent]
})
export class ResetPasswordModule {
}
